package com.epam.courses.oop.exception;

public class TrackNotFoundException extends RuntimeException {

    public TrackNotFoundException() {
        super();
    }

    public TrackNotFoundException(String message) {
        super(message);
    }

    public TrackNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TrackNotFoundException(Throwable cause) {
        super(cause);
    }
}
