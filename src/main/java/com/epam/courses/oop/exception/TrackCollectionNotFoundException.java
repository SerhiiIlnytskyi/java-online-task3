package com.epam.courses.oop.exception;

public class TrackCollectionNotFoundException extends RuntimeException {
    public TrackCollectionNotFoundException() {
        super();
    }

    public TrackCollectionNotFoundException(String message) {
        super(message);
    }

    public TrackCollectionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TrackCollectionNotFoundException(Throwable cause) {
        super(cause);
    }
}
