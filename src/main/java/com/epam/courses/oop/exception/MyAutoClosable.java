package com.epam.courses.oop.exception;

public class MyAutoClosable implements AutoCloseable {
    @Override
    public void close() throws Exception {
        System.out.println("Autoclosable works");
    }
}
