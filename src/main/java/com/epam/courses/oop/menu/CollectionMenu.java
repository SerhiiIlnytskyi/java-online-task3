package com.epam.courses.oop.menu;

import com.epam.courses.oop.model.TrackCollection;

import java.io.IOException;

import static com.epam.courses.oop.menu.MainMenu.*;

public enum CollectionMenu implements MenuService {

    BACK("Return to the main menu") {
        @Override
        public void service() {
            try {
                MainMenu.runMainMenu();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    },
    LIST_COLLECTIONS("Show all music playlists and choose current") {
        @Override
        public void service() {
            if (trackCollectionList.size() > 0) {
                System.out.print(DIVIDER + "\n-> Choose one music playlist: ");

                for (int i = 0; i < trackCollectionList.size(); i++) {
                    System.out.println(i + ". " + trackCollectionList.get(i));
                }
                try {
                    currentTrackCollection = trackCollectionList.get(Integer.parseInt(bufferedReader.readLine()));
                } catch (IOException e) {
                    System.out.println(INCORRECT_INPUT);
                }
            } else {
                System.out.println(DIVIDER + "\n-> List with music playlists is empty. Please add playlist");
            }
        }
    },
    ADD_COLLECTION("Add new music playlist") {
        @Override
        public void service() {
            System.out.print(DIVIDER + "\n-> Please write playlist name: ");

            String name = null;
            try {
                name = bufferedReader.readLine();
                TrackCollection trackCollection = new TrackCollection(name);
                trackCollectionList.add(trackCollection);
                System.out.printf("-> Playlist named %s created. Do you want to make it current? (y/n) : ", name);
                String answer = bufferedReader.readLine();
                if (answer.equals("y")) {
                    currentTrackCollection = trackCollection;
                }
            } catch (IOException e) {
                System.out.println(INCORRECT_INPUT);
            }
        }
    };

    private String menuItemDescription;

    CollectionMenu(String menuItemDescription) {
        this.menuItemDescription = menuItemDescription;
    }

    public static void runCollectionMenu() throws IOException {
        while (true) {
            writeMenu();
            String inputText = bufferedReader.readLine();

            switch (inputText) {
                case "0":
                    CollectionMenu.BACK.service();
                    return;
                case "1":
                    CollectionMenu.LIST_COLLECTIONS.service();
                    break;
                case "2":
                    CollectionMenu.ADD_COLLECTION.service();
                    break;
                default:
                    System.out.println(WRONG_CHOICE);
            }

        }
    }

    public static void writeMenu() {
        System.out.println(DIVIDER);
        if (currentTrackCollection != null) {
            System.out.println("!! Current playlist name is: " + currentTrackCollection.getName());
            System.out.printf("!! There are %d tracks \n" + DIVIDER + "\n", currentTrackCollection.getTracks().size());
        } else {
            System.out.println("-> You didn't choose any playlist. Please choose or create one \n" + DIVIDER);
        }
        System.out.println("PLAYLISTS_MENU: ");
        for (int i = 0; i < CollectionMenu.values().length; i++) {
            System.out.println(CollectionMenu.values()[i].ordinal() + ". " + CollectionMenu.values()[i].menuItemDescription);
        }
        System.out.print(DIVIDER + "\n-> Please select a number from menu: ");
    }

    public String getMenuItemDescription() {
        return menuItemDescription;
    }

    public void setMenuItemDescription(String menuItemDescription) {
        this.menuItemDescription = menuItemDescription;
    }
}
