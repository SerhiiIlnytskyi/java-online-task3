package com.epam.courses.oop.menu;

import com.epam.courses.oop.model.MusicStyle;
import com.epam.courses.oop.model.Track;

import java.io.IOException;
import java.util.List;

import static com.epam.courses.oop.menu.MainMenu.*;
import static com.epam.courses.oop.menu.MainMenu.DIVIDER;

public enum TrackMenu implements MenuService {

    BACK("Return to the main menu") {
        @Override
        public void service() {
            try {
                MainMenu.runMainMenu();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    },
    ADD_TRACK("Add new track to current playlist") {
        @Override
        public void service() {
            if (currentTrackCollection != null) {
                try {
                    System.out.print(DIVIDER + "\n-> Please write track name: ");
                    String trackName = bufferedReader.readLine();

                    System.out.print("-> Please write track performer: ");
                    String trackPerformer = bufferedReader.readLine();

                    for (int i = 0; i < MusicStyle.values().length; i++) {
                        System.out.println(i + ". " + MusicStyle.values()[i]);
                    }
                    System.out.print("-> Please choose track style: ");
                    MusicStyle musicStyle = MusicStyle.values()[Integer.parseInt(bufferedReader.readLine())];

                    System.out.print("-> Please write track duration in seconds: ");
                    Integer trackduration = Integer.parseInt(bufferedReader.readLine());

                    currentTrackCollection.addTrack(new Track(trackName, trackPerformer, musicStyle, trackduration));
                } catch (IOException e) {
                    System.out.println(INCORRECT_INPUT);
                }
            } else {
                System.out.println("! Please choose playlist first.");
            }
        }
    },
    REMOVE_TRACK("Find track by name and remove it from playlist") {
        @Override
        public void service() {
            System.out.print("-> Write here track name to delete: ");
            try {
                String name = bufferedReader.readLine();
                Track track = currentTrackCollection.findTrackByName(name);
                if ((track != null) && (currentTrackCollection.removeTrack(track))) {
                    System.out.printf("Track %s removed successfully \n", track);
                } else {
                    System.out.printf("Track %s didn't remove \n", track);
                }
            } catch (IOException e) {
                System.out.println(INCORRECT_INPUT);
            }
        }
    },
    LIST_TRACKS("List all tracks of the current playlist") {
        @Override
        public void service() {
            if (currentTrackCollection != null) {
                for (int i = 0; i < currentTrackCollection.getTracks().size(); i++) {
                    System.out.println(currentTrackCollection.getTracks().get(i));
                }
            } else {
                System.out.println("-> Please choose playlist first");
            }
        }
    },
    SORT_TRACKS("Sort tracks in the current playlist") {
        @Override
        public void service() {
            currentTrackCollection.sortByStyle();
            System.out.println("! Music playlist sorted");
        }
    },
    CHOOSE_TRACKS_WITH_DURATION("Choose tracks by min and max duration") {
        @Override
        public void service() {
            List<Track> trackList = null;

            try {
                System.out.print("Choose minimum duration of tracks (in seconds): ");
                Integer minimumDuration = Integer.parseInt(bufferedReader.readLine());
                System.out.print("Choose maximum duration of tracks (in seconds): ");
                Integer maximumDuration = Integer.parseInt(bufferedReader.readLine());
                trackList = currentTrackCollection.findByDurationRange(minimumDuration, maximumDuration);
            } catch (IOException e) {
                System.out.println(INCORRECT_INPUT);
            }

            if (trackList != null) {
                System.out.println("! Resulted list: ");
                for (int i = 0; i < trackList.size(); i++) {
                    System.out.println(trackList.get(i));
                }
            }

        }
    };

    private String menuItemDescription;

    TrackMenu(String menuItemDescription) {
        this.menuItemDescription = menuItemDescription;
    }

    public static void runTrackMenu() throws IOException {
        while (true) {
            writeMenu();
            String inputText = bufferedReader.readLine();

            switch (inputText) {
                case "0":
                    TrackMenu.BACK.service();
                    return;
                case "1":
                    TrackMenu.ADD_TRACK.service();
                    break;
                case "2":
                    TrackMenu.REMOVE_TRACK.service();
                    break;
                case "3":
                    TrackMenu.LIST_TRACKS.service();
                    break;
                case "4":
                    TrackMenu.SORT_TRACKS.service();
                    break;
                case "5":
                    TrackMenu.CHOOSE_TRACKS_WITH_DURATION.service();
                    break;
                default:
                    System.out.println(WRONG_CHOICE);
            }

        }
    }

    public static void writeMenu() {
        System.out.println(DIVIDER);
        if (currentTrackCollection != null) {
            System.out.println("!! Current playlist name is: " + currentTrackCollection.getName());
            System.out.printf("!! There are %d tracks \n" + DIVIDER + "\n", currentTrackCollection.getTracks().size());
        } else {
            System.out.println("-> You didn't choose any playlist. Please choose or create  one \n" + DIVIDER);
        }
        System.out.println("TRACKS_MENU: ");
        for (int i = 0; i < TrackMenu.values().length; i++) {
            System.out.println(TrackMenu.values()[i].ordinal() + ". " + TrackMenu.values()[i].menuItemDescription);
        }
        System.out.print(DIVIDER + "\n-> Please select a number from menu: ");
    }

    public String getMenuItemDescription() {
        return menuItemDescription;
    }

    public void setMenuItemDescription(String menuItemDescription) {
        this.menuItemDescription = menuItemDescription;
    }
}
