package com.epam.courses.oop.menu;

import com.epam.courses.oop.model.TrackCollection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum MainMenu implements MenuService {
    EXIT("EXIT") {
        @Override
        public void service() {
            System.out.println(GOOD_LUCK);
            try {
                bufferedReader.close();
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    },
    COLLECTIONS_MENU("Go to the track playlists menu") {
        @Override
        public void service() {
            try {
                CollectionMenu.runCollectionMenu();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    },
    TRACKS_MENU("Go to the tracks menu") {
        @Override
        public void service() {
            if (currentTrackCollection != null) {
                try {
                    TrackMenu.runTrackMenu();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("!! First, you must choose current playlist to work with.");
            }
        }
    },
    PLAY_MUSIC_MENU("Play music from playlists") {
        @Override
        public void service() {
            System.out.println("!! We are working on it !!");
        }
    };

    public static final String INCORRECT_INPUT =
            "---------------------------------Incorrect input----------------------------------";
    public static final String DIVIDER =
            "----------------------------------------------------------------------------------";
    public static final String WRONG_CHOICE =
            "----------------------------------------------------------------------------------\n" +
            "---------------------WRONG CHOICE. Please select a number again-------------------";
    public static final String GOOD_LUCK =
            "------------------------------------Good luck-------------------------------------";

    protected static List<TrackCollection> trackCollectionList = new ArrayList<>();
    protected static TrackCollection currentTrackCollection;
    protected static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    private String menuItemDescription;

    MainMenu(String s) {
        this.menuItemDescription = s;
    }

    public static void writeMenu() {
        System.out.println(DIVIDER);
        if (currentTrackCollection != null) {
            System.out.println("!! Current playlist name is: " + currentTrackCollection.getName());
            System.out.printf("!! There are %d tracks \n" + DIVIDER + "\n", currentTrackCollection.getTracks().size());
        } else {
            System.out.println("!! You didn't choose any playlist. Please choose or create one\n" + DIVIDER);
        }
        System.out.println("MENU: ");
        for (int i = 0; i < values().length; i++) {
            System.out.println(values()[i].ordinal() + ". " + values()[i].menuItemDescription);
        }
        System.out.print(DIVIDER + "\n-> Please select a number from menu: ");
    }

    public static void runMainMenu() throws IOException {
        while (true) {
            MainMenu.writeMenu();
            String inputText = bufferedReader.readLine();

            switch (inputText) {
                case "0":
                    MainMenu.EXIT.service();
                    break;
                case "1":
                    MainMenu.COLLECTIONS_MENU.service();
                    break;
                case "2":
                    MainMenu.TRACKS_MENU.service();
                    break;
                case "3":
                    MainMenu.PLAY_MUSIC_MENU.service();
                    break;
                default:
                    System.out.println(WRONG_CHOICE);
            }

        }
    }

    public static List<MainMenu> menuItems() {
        List<MainMenu> items = Arrays.asList(MainMenu.values());
        return items;
    }

    public String getMenuItemDescription() {
        return menuItemDescription;
    }

    public void setMenuItemDescription(String menuItemDescription) {
        this.menuItemDescription = menuItemDescription;
    }
}
