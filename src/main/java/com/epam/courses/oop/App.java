package com.epam.courses.oop;

import com.epam.courses.oop.exception.MyAutoClosable;
import com.epam.courses.oop.menu.MainMenu;
import com.epam.courses.oop.menu.MenuService;

public class App {
    public static void main(String[] args) throws Exception {
        MainMenu.runMainMenu();

        try (MyAutoClosable autoClosable = new MyAutoClosable()){

        }

    }
}
