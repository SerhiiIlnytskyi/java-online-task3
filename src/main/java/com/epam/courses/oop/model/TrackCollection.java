package com.epam.courses.oop.model;

import com.epam.courses.oop.exception.TrackNotFoundException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TrackCollection {

    private String name;

    private List<Track> tracks = new ArrayList<>();

    public void sortByStyle() {
        this.tracks =  this.tracks.stream()
                .sorted(Comparator.comparing(Track::getStyle))
                .collect(Collectors.toList());
    }

    public void sortByName() {
        this.tracks =  this.tracks.stream()
                .sorted(Comparator.comparing(Track::getName))
                .collect(Collectors.toList());
    }

    public List<Track> findByDurationRange(int fromDuration, int toDuration) {
        return this.tracks.stream()
                .filter(e -> (e.getDuration() >= fromDuration) && (e.getDuration() <= toDuration))
                .collect(Collectors.toList());
    }

    public Track findTrackByName(String name) {
        return this.tracks.stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new TrackNotFoundException("Not found track with name: " + name));
    }

    public void addTrack(Track track) {
        this.tracks.add(track);
    }

    public boolean removeTrack(Track track) {
        if (this.tracks.contains(track)) {
            this.tracks.remove(track);
            return true;
        } else {
            throw new TrackNotFoundException("Not found track : " + track);
        }
    }

    public TrackCollection(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    @Override
    public String toString() {
        return "TrackCollection{" +
                "name='" + name + '\'' +
                ", tracks=" + tracks +
                '}';
    }
}
