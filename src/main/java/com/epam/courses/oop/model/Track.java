package com.epam.courses.oop.model;

import java.io.File;
import java.util.Objects;

public class Track {

    private String name;

    private String performer;

    private int duration;

    private File file;

    private MusicStyle style;

    public Track(String name, String performer, MusicStyle style) {
        this.name = name;
        this.performer = performer;
        this.style = style;
    }

    public Track(String trackName, String trackPerformer, MusicStyle musicStyle, Integer trackduration) {
        this.name = trackName;
        this.performer = trackPerformer;
        this.style = musicStyle;
        this.duration = trackduration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public MusicStyle getStyle() {
        return style;
    }

    public void setStyle(MusicStyle style) {
        this.style = style;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Track track = (Track) o;
        return duration == track.duration &&
                name.equals(track.name) &&
                Objects.equals(performer, track.performer) &&
                file.equals(track.file) &&
                style == track.style;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, performer, duration, file, style);
    }

    @Override
    public String toString() {
        return "Track{" +
                "name='" + name + '\'' +
                ", performer='" + performer + '\'' +
                ", duration=" + duration +
                ", file=" + file +
                ", style=" + style +
                '}';
    }
}
