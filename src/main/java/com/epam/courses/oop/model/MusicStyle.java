package com.epam.courses.oop.model;

public enum MusicStyle {
    ROCK,
    JAZZ,
    CLASSIC,
    FOLK,
    BLUES,
    POP,
    RAP
}
