package com.epam.courses.exceptions;

import com.epam.courses.oop.exception.MyAutoClosable;
import com.epam.courses.oop.menu.MainMenu;

public class App {
    public static void main(String[] args) throws Exception {

        try (MyAutoClosable autoClosable = new MyAutoClosable()){

        }

    }
}
